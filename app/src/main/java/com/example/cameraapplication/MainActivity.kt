package com.example.cameraapplication

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import kotlinx.android.synthetic.main.activity_main.*
import java.io.*
import java.lang.reflect.Type

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        supportActionBar?.setDisplayShowTitleEnabled(false)

        setContentView(R.layout.activity_main)

        val retornoIntent  = intent.getStringExtra("response")
        val tempoIntent = intent.getStringExtra("tempo")

        btn_photo.setOnClickListener {
            val intent = Intent(this, CameraActivity::class.java)
            startActivity(intent)
            finish()
        }

        println("Retorno Intent: "+retornoIntent)
        if(retornoIntent != null){
            var data: List<Response> = mutableListOf()
            val REVIEW_TYPE: Type = object : TypeToken<List<Response?>?>() {}.type
            val gson = Gson()
            val reader = JsonReader(FileReader(retornoIntent))
            data = gson.fromJson(reader, REVIEW_TYPE) // contains the whole reviews list
            println("Data: \n$data")
            var img=""
            var str = tempoIntent
            for(i: Response in data){
                if(i.nome =="NotFound"){

                    val pos = i.imagem.toString().lastIndexOf("/")
                    val directory = i.imagem.toString().substring(0,pos)+"/"
                    val file = i.imagem.toString().substring(pos+1,i.imagem.toString().length)

                    try {
                        moveFile(directory, file, "$directory/unrecognized/notfound_")
                    } catch(ex: Exception){
                        ex.printStackTrace()
                        Toast.makeText(this,"Não foi possível mover o arquivo",Toast.LENGTH_LONG).show()
                    }

                    Toast.makeText(this,"Arquivo movido com sucesso!",Toast.LENGTH_LONG).show()

                    str += "\nNão foi encontrada nenhuma face nessa imagem"
                    if(img.isEmpty()){
                        img=i.imagem.toString()
                    }
                } else{
                    if(i.nome.isNullOrEmpty() || i.nome.isNullOrBlank()){
                        val pos = i.imagem.toString().lastIndexOf("/")
                        val directory = i.imagem.toString().substring(0,pos)+"/"
                        val file = i.imagem.toString().substring(pos+1,i.imagem.toString().length)

                        try {
                            moveFile(directory, file, "$directory/unrecognized/")
                        } catch(ex: Exception){
                            ex.printStackTrace()
                            Toast.makeText(this,"Não foi possível mover o arquivo",Toast.LENGTH_LONG).show()
                        }

                        Toast.makeText(this,"Arquivo movido com sucesso!",Toast.LENGTH_LONG).show()
                        str+="\nNão foi possível reconhecer esse animal \nDistância: ${i.distancia}"
                    } else{
                        str += "\nAnimal: ${i.nome}\n\tDistancia: ${i.distancia}\n"
                    }
                    if(img.isEmpty()){
                        img=i.imagem.toString()
                    }
                }
            }
            tv_resposta.text = str
            iv_photo_saved.setImageURI(Uri.parse(img))
        }
    }

    private fun moveFile(inputPath: String, inputFile: String, outputPath: String) {
        var `in`: InputStream? = null
        var out: OutputStream? = null
        try {

            //create output directory if it doesn't exist
            val dir = File(outputPath)
            if (!dir.exists()) {
                dir.mkdirs()
            }
            `in` = FileInputStream(inputPath + inputFile)
            out = FileOutputStream(outputPath + inputFile)
            val buffer = ByteArray(1024)
            var read: Int
            while (`in`.read(buffer).also { read = it } != -1) {
                out.write(buffer, 0, read)
            }
            `in`.close()
            `in` = null

            // write the output file
            out.flush()
            out.close()
            out = null

            // delete the original file
            File(inputPath + inputFile).delete()
        } catch (fnfe1: FileNotFoundException) {
            Log.e("tag", fnfe1.message.toString())
        } catch (e: java.lang.Exception) {
            Log.e("tag", e.message!!)
        }
    }

    private fun deleteFile(inputPath: String, inputFile: String) {
        try {
            // delete the original file
            File(inputPath + inputFile).delete()
        } catch (e: java.lang.Exception) {
            Log.e("tag", e.message!!)
        }
    }

    data class Response(
        val imagem: String?,
        val nome: String?,
        val distancia: Double?
    ) {
        override fun toString(): String {
            return "(imagem=$imagem,\nnome=$nome, \ndistancia=$distancia)"
        }
    }
}