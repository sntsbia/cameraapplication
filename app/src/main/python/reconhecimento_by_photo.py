import os
import glob
import _pickle as cPickle
import dlib
import cv2
import numpy as np
import os.path
from os.path import dirname,join
import json
from time import gmtime, strftime

def mainFunction(imgUri):
    print("URI: ", imgUri)

    #Local dos arquivos no sistema
    svm_directory = join(dirname(__file__),"recursos","detector_vacas_faces_v13_cel.svm")
    dat_pontos_directory = join(dirname(__file__),"recursos","detector_vacas_face_5pontos.dat")
    resnet_dat_directory = join(dirname(__file__),"recursos","dlib_face_recognition_resnet_model_v1.dat")
    indices_pickle_directory = join(dirname(__file__),"recursos","indices_rn_vaca_cel.pickle")
    descritores_npy_directory = join(dirname(__file__),"recursos","descritores_rn_vaca_cel.npy")
    json_dir = join(dirname(__file__),"jsons")

    detectorFace = dlib.simple_object_detector(svm_directory)
    detectorPontos = dlib.shape_predictor(dat_pontos_directory)
    reconhecimentoFacial = dlib.face_recognition_model_v1(resnet_dat_directory)
    indices = np.load(indices_pickle_directory,allow_pickle=True)
    descritoresFaciais = np.load(descritores_npy_directory)
    limiar = 0.32

    imagem = cv2.imread(imgUri)
    facesDetectadas = detectorFace(imagem, 1)
    print("Faces detectadas: ",len(facesDetectadas))
    map_values = []
    for face in facesDetectadas:
        e, t, d, b = (int(face.left()), int(face.top()), int(face.right()), int(face.bottom()))
        pontosFaciais = detectorPontos(imagem, face)
        descritorFacial = reconhecimentoFacial.compute_face_descriptor(imagem, pontosFaciais)
        listaDescritorFacial = [fd for fd in descritorFacial]
        npArrayDescritorFacial = np.asarray(listaDescritorFacial, dtype=np.float64)
        npArrayDescritorFacial = npArrayDescritorFacial[np.newaxis, :]

        distancias = np.linalg.norm(npArrayDescritorFacial - descritoresFaciais, axis=1)
        print("Distâncias: {}".format(distancias))
        minimo = np.argmin(distancias)
        print(minimo)
        distanciaMinima = distancias[minimo]
        print(distanciaMinima)

        if distanciaMinima <= limiar:
            nome = os.path.split(indices[minimo])[1].split(".")[0]
        else:
            nome = ' '

        texto = "{} {:.4f}".format(" ",distanciaMinima)


        map = {"imagem":imgUri,"nome":nome,"distancia":texto}
        map_values.append(map)

    if len(facesDetectadas) == 0:
        map_n_encontrou = {"imagem":imgUri,"nome":"NotFound","distancia":0}
        map_values.append(map_n_encontrou)
#         print("Foto: ",imagem,"Animal: ",nome,"Texto: ",texto)
    #     Caminho para gerar json



    arquivo = dirname(__file__)+"/jsons/"+strftime("%Y%m%d-%H%M%S", gmtime())+".json"
    print("### ", svm_directory)
    print("#### ",arquivo)

    print(map_values)
    with open(arquivo, "w", encoding='utf-8') as outfile:
        json.dump(map_values, outfile, ensure_ascii=False, indent=2)

    print(map_values)

    return arquivo
